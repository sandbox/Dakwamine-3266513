<?php

namespace Drupal\decoupled_toolbox;

/**
 * Represents a field value and its options.
 *
 * This is used when a decoupled field formatter has to bind field values and
 * options (such as location) in a way that real data and meta data (those
 * options) do not mix.
 */
class FieldValueAndOptions {

  /**
   * Decoupled field key.
   *
   * @var string
   */
  private $decoupledFieldKey;

  /**
   * Options array.
   *
   * @var array
   */
  private $decoupledFieldLocation;

  /**
   * The real value of the field.
   *
   * @var mixed
   */
  private $value;

  /**
   * FieldValueAndOptions constructor.
   *
   * @param string $decoupledFieldKey
   *   The decoupled field key.
   * @param mixed $value
   *   The real value of the field.
   * @param string $decoupledFieldLocation
   *   The field location override, if any.
   */
  public function __construct($decoupledFieldKey, $value, $decoupledFieldLocation) {
    $this->decoupledFieldKey = $decoupledFieldKey;
    $this->decoupledFieldLocation = $decoupledFieldLocation;
    $this->value = $value;
  }

  /**
   * The decoupled field key.
   *
   * @return string
   *   Field key.
   */
  public function getDecoupledFieldKey() {
    return $this->decoupledFieldKey;
  }

  /**
   * The field decoupled location. May be empty if no override is needed.
   *
   * @return mixed
   *   Field decoupled location.
   */
  public function getDecoupledFieldLocation() {
    return $this->decoupledFieldLocation;
  }

  /**
   * The field value.
   *
   * @return mixed
   *   Field value.
   */
  public function getValue() {
    return $this->value;
  }

}
