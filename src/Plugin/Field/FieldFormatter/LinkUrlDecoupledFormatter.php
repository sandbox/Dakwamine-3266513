<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Plugin implementation of the "decoupled_link_url" formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_link_url",
 *   label = @Translation("Link URL decoupled formatter"),
 *   field_types = {
 *     "link",
 *   }
 * )
 */
class LinkUrlDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    /* @var \Drupal\link\LinkItemInterface $item */
    return $item->getUrl()->toString();
  }

}
