<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Plugin implementation of the 'decoupled_generic' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_generic",
 *   label = @Translation("Generic decoupled formatter"),
 *   field_types = {
 *     "changed",
 *     "created",
 *     "comment",
 *     "daterange",
 *     "datetime",
 *     "decimal",
 *     "email",
 *     "file_uri",
 *     "float",
 *     "integer",
 *     "language",
 *     "list_float",
 *     "list_integer",
 *     "list_string",
 *     "map",
 *     "path",
 *     "password",
 *     "string",
 *     "string_long",
 *     "timestamp",
 *     "telephone",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "uri",
 *     "uuid",
 *   }
 * )
 */
class GenericDecoupledFormatter extends DecoupledFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, $langCode) {
    $this->validateSettingsOnRender();

    $decoupledFieldKey = $this->getDecoupledFieldKey();
    $decoupledFieldLocation = $this->getDecoupledFieldLocation();

    if ($this->isMultivaluedFieldItemList($items)) {
      $processedMultiValuedItems = $this->viewMultivaluedFieldItemList($items);

      if (empty($processedMultiValuedItems)) {
        // No value set.
        $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
        $this->setProcessedFieldValue($fieldValue);
        return $fieldValue;
      }

      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, $processedMultiValuedItems, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    try {
      /* @var \Drupal\Core\Field\FieldItemInterface $item */
      $item = $items->first();
    }
    catch (MissingDataException $exception) {
      // No value set.
      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    if (empty($item)) {
      // No value set.
      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    $fieldValue = new FieldValueAndOptions($decoupledFieldKey, $this->viewFieldItem($item), $decoupledFieldLocation);
    $this->setProcessedFieldValue($fieldValue);
    return $fieldValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return $this->escapeOutput($item->value);
  }

  /**
   * {@inheritdoc}
   */
  protected function viewMultivaluedFieldItemList(FieldItemListInterface $items) {
    $data = [];

    foreach ($items as $delta => $item) {
      $data[$delta] = $this->viewFieldItem($item);
    }

    return $data;
  }

}
