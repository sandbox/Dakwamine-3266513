<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Plugin implementation of the 'decoupled_generic_raw' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_generic_raw",
 *   label = @Translation("Generic raw decoupled formatter"),
 *   field_types = {
 *     "changed",
 *     "created",
 *     "comment",
 *     "daterange",
 *     "datetime",
 *     "decimal",
 *     "email",
 *     "file_uri",
 *     "float",
 *     "integer",
 *     "language",
 *     "list_float",
 *     "list_integer",
 *     "list_string",
 *     "map",
 *     "path",
 *     "password",
 *     "string",
 *     "string_long",
 *     "timestamp",
 *     "telephone",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "uri",
 *     "uuid",
 *   }
 * )
 */
class RawGenericDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    return $item->value;
  }

}
