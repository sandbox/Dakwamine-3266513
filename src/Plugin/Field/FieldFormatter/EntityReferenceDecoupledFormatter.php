<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\decoupled_toolbox\Service\DecoupledRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'decoupled_entity_reference' formatter.
 *
 * Some parts inspired by the core entity reference formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_entity_reference",
 *   label = @Translation("Entity reference decoupled formatter"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   }
 * )
 */
class EntityReferenceDecoupledFormatter extends EntityReferenceDecoupledFormatterBase {

  const SETTINGS__DECOUPLED_ENTITY_VIEW_MODE = 'decoupled_entity_view_mode';

  /**
   * Decoupled renderer service.
   *
   * @var \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface
   */
  protected $decoupledRenderer;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a EntityReferenceDecoupledFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface $decoupledRenderer
   *   Decoupled renderer service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    DecoupledRendererInterface $decoupledRenderer,
    EntityDisplayRepositoryInterface $entityDisplayRepository
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->decoupledRenderer = $decoupledRenderer;
    $this->entityDisplayRepository = $entityDisplayRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @var \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface $decoupledRenderer */
    $decoupledRenderer = $container->get('decoupled.renderer');

    /* @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository */
    $entityDisplayRepository = $container->get('entity_display.repository');

    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $decoupledRenderer,
      $entityDisplayRepository);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::SETTINGS__DECOUPLED_ENTITY_VIEW_MODE => 'decoupled',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form[static::SETTINGS__DECOUPLED_ENTITY_VIEW_MODE] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('View mode'),
      '#default_value' => $this->getSetting(static::SETTINGS__DECOUPLED_ENTITY_VIEW_MODE),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $viewModes = $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type'));
    $viewMode = $this->getSetting(static::SETTINGS__DECOUPLED_ENTITY_VIEW_MODE);
    $summary[] = $this->t('Rendered as @mode', ['@mode' =>$viewModes[$viewMode] ?? $viewMode]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    if (!$item instanceof EntityReferenceItem) {
      throw new UnexpectedFormatterException('Tried to render an entity field item, but the given object does not implement EntityReferenceItem.');
    }

    // $fieldValue is supposed to be an array containing at least the target_id
    // key, and the target_revision_id for entity reference revisions fields.
    $fieldValue = $item->getValue();

    if (empty($fieldValue)) {
      // This should never be possible.
      throw new InvalidContentException('Field value is empty.');
    }

    /* @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $item->entity;

    if (empty($entity)) {
      // This happens when the reference was deleted.
      throw new InvalidContentException('Reference was deleted.');
    }

    $entityTypeId = $entity->getEntityTypeId();

    $viewMode = $this->getSetting(static::SETTINGS__DECOUPLED_ENTITY_VIEW_MODE);
    $vieMode = !empty($viewMode) ? $viewMode : 'decoupled';

    try {
      $cacheTags = [];
      $rendered = $this->decoupledRenderer->renderByEntityTypeAndId($entityTypeId, $entity->id(),  $cacheTags,$vieMode);
    }
    catch (\Exception $exception) {
      // Unrenderable entity.
      throw new InvalidContentException('Unrenderable entity.');
    }

    foreach ($cacheTags as $tag) {
      $this->addProcessedFieldCacheTag($tag);
    }

    return $rendered;
  }

}
