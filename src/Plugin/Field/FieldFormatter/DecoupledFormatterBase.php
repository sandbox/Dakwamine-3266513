<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_toolbox\Exception\FieldNotYetProcessedException;
use Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Abstract of a decoupled formatter.
 */
abstract class DecoupledFormatterBase extends FormatterBase implements DecoupledFormatterInterface {

  const SETTINGS__DECOUPLED_FIELD_KEY = 'decoupled_field_key';
  const SETTINGS__DECOUPLED_FIELD_LOCATION = 'decoupled_field_location';

  /**
   * Indicates if this formatter has processed a field.
   *
   * @var bool
   */
  private $hasProcessedField = FALSE;

  /**
   * Formatted field value.
   *
   * @var \Drupal\decoupled_toolbox\FieldValueAndOptions
   */
  private $processedFieldValue;

  /**
   * Cache tags array for the processed field.
   *
   * @var array
   */
  private $processedFieldCacheTags;

  /**
   * Adds a cache tag associated to this field formatter.
   *
   * Call this when the field formatter has processed content which needs cache
   * tag for invalidation.
   *
   * @param string $tag
   *   The cache tag to add for the processed field.
   */
  protected function addProcessedFieldCacheTag($tag) {
    $this->processedFieldCacheTags[] = $tag;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      self::SETTINGS__DECOUPLED_FIELD_KEY => '',
      self::SETTINGS__DECOUPLED_FIELD_LOCATION => '',
    ] + parent::defaultSettings();
  }

  /**
   * Standard text escape for decoupled formatters.
   *
   * You may override this function to change or disable escapes in inheriting
   * classes.
   *
   * @param string $stringToEscape
   *   String to escape.
   *
   * @return string
   *   String escaped for output.
   */
  protected function escapeOutput($stringToEscape) {
    return nl2br(Html::escape($stringToEscape));
  }

  /**
   * Shortcut to get decoupled_field_key value.
   *
   * @return string
   *   The decoupled_field_key value.
   */
  protected function getDecoupledFieldKey() {
    return $this->settings[self::SETTINGS__DECOUPLED_FIELD_KEY];
  }

  /**
   * Shortcut to get decoupled_field_location value.
   *
   * @return string
   *   The decoupled_field_location value.
   */
  protected function getDecoupledFieldLocation() {
    return $this->settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION];
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessedFieldCacheTags() {
    if (!$this->hasProcessedField()) {
      throw new FieldNotYetProcessedException();
    }

    if (empty($this->processedFieldCacheTags)) {
      // This may happen if the child formatter has not set any cache tags.
      return [];
    }

    return $this->processedFieldCacheTags;
  }

  /**
   * Gets the settings summary array.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Array of markup.
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    /* @var \Drupal\Core\StringTranslation\TranslatableMarkup[] $summary */
    $summary = parent::settingsSummary();

    if (!empty($settings[self::SETTINGS__DECOUPLED_FIELD_KEY])) {
      $summary[] = $this->t('Decoupled field ID: <strong>@id</strong>', ['@id' => $settings[self::SETTINGS__DECOUPLED_FIELD_KEY]]);
    }
    else {
      $summary[] = $this->t('⚠️ Decoupled field ID: <em>️undefined</em>');
    }

    if (!empty($settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION])) {
      $summary[] = $this->t('Decoupled field location: <strong>@location</strong>', ['@location' => $settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION]]);
    }

    return $summary;
  }

  /**
   * Tells if this formatter has processed a field or not.
   *
   * @return bool
   *   TRUE if field has been processed. FALSE otherwise.
   */
  protected function hasProcessedField() {
    return $this->hasProcessedField;
  }

  /**
   * Checks whether a FieldItemList instance comes from a multi-valued field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The FieldItemList instance to test.
   *
   * @return bool
   *   TRUE if multi-valued, FALSE if single valued.
   */
  protected function isMultivaluedFieldItemList(FieldItemListInterface $items) {
    return $items->getFieldDefinition()->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * Sets the processed field value on this formatter instance.
   *
   * @param \Drupal\decoupled_toolbox\FieldValueAndOptions $value
   *   The processed field value which can be used by the decoupled renderer.
   */
  protected function setProcessedFieldValue(FieldValueAndOptions $value) {
    $this->processedFieldValue = $value;
    $this->hasProcessedField = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form[self::SETTINGS__DECOUPLED_FIELD_KEY] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FIELD_KEY),
      '#description' => $this->t('This value replaces the Drupal field ID by a suitable key for the decoupled display. Supports any character as defined by the JSON key syntax.'),
      '#required' => TRUE,
      '#title' => $this->t('Decoupled field key'),
      '#type' => 'textfield',
    ];

    $form[self::SETTINGS__DECOUPLED_FIELD_LOCATION] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FIELD_LOCATION),
      '#description' => $this->t('Location of this field. Leave empty for default position. Examples: <ul><li><strong>/absolute/location/in/final/output</strong>: absolute position, the field will be placed in the object keyed by "output"</li><li><strong>/0/a_key_name</strong>: inside an object keyed by "a_key_name" in the first object of the output.</li></ul>'),
      '#title' => $this->t('Decoupled field location'),
      '#type' => 'textfield',
    ];

    return $form;
  }

  /**
   * Validates the formatter settings when building the output.
   *
   * Override this in inheriting classes if the formatter needs to validate
   * other settings.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   */
  protected function validateSettingsOnRender() {
    $settings = $this->getSettings();

    if (empty($settings[self::SETTINGS__DECOUPLED_FIELD_KEY])) {
      throw new InvalidFormatterSettingsException();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Return an empty array because we are not using the default render stack.
    return [];
  }

  /**
   * Processes a single field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The formatted value as string.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   An error occurred about the content of the field.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   * @throws \Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException
   *   The formatter is not compatible with the field.
   */
  abstract protected function viewFieldItem(FieldItemInterface $item);

  /**
   * Processes field with cardinality > 1.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Items to format.
   *
   * @return array
   *   The formatted items as an array.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   An error occurred about the content of the field.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   * @throws \Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException
   *   The formatter is not compatible with the field.
   */
  abstract protected function viewMultivaluedFieldItemList(FieldItemListInterface $items);

}
