<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'decoupled_generic_image' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_generic_image",
 *   label = @Translation("Generic image decoupled formatter"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class ImageGenericDecoupledFormatter extends FileDecoupledFormatter {

  const IMAGE_SOURCE = "gam";

  protected $streamWrapperManager;

  /**
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    StreamWrapperManagerInterface $stream_wrapper_manager) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings);
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /* @var StreamWrapperManagerInterface $stream_wrapper_manager */
    $stream_wrapper_manager = $container->get('stream_wrapper_manager');

    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $stream_wrapper_manager
    );
  }


  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    if (!$item instanceof ImageItem) {
      throw new UnexpectedFormatterException($this->t("Tried to render an image item, but the given object does not implement ImageItem."));
    }

    // $fieldValue is supposed to be an array containing at least the target_id
    // key and other values related to an image (width, height, alt, title).
    $fieldValue = $item->getValue();

    if (empty($fieldValue)) {
      // This should never be possible.
      throw new InvalidContentException($this->t("Field value is empty."));
    }

    /* @var \Drupal\file\FileInterface $file */
    $file = $item->entity;

    if (empty($file)) {
      // This happens when the reference was deleted.
      throw new InvalidContentException($this->t("Reference was deleted."));
    }

    // Remove the scheme from the URI and remove erroneous leading or trailing.
    $uri = $this->streamWrapperManager::getTarget($file->getFileUri());

    foreach ($file->getCacheTags() as $tag) {
      $this->addProcessedFieldCacheTag($tag);
    }

    return array_merge([
      "source" => self::IMAGE_SOURCE,
      "uri" => $uri,
    ], $this->getCommonImageProperties($item));
  }

  /**
   * Gets generic common properties such as title, alt.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $item
   *   The ImageItem currently processed by the processor.
   *
   * @return array
   *   Array containing string indexed values.
   */
  private function getCommonImageProperties(ImageItem $item) {
    try {
      $title = $item->get("title")->getValue();
    }
    catch (MissingDataException $exception) {
      $title = NULL;
    }

    try {
      $alt = $item->get("alt")->getValue();
    }
    catch (MissingDataException $exception) {
      $alt = NULL;
    }
    return [
      "title" => $title,
      "alt" => $alt,
    ];
  }

}
