<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Base plugin implementation of the decoupled entity formatters.
 */
abstract class EntityReferenceDecoupledFormatterBase extends DecoupledFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, $langCode) {
    $this->validateSettingsOnRender();

    $decoupledFieldKey = $this->getDecoupledFieldKey();
    $decoupledFieldLocation = $this->getDecoupledFieldLocation();

    if ($this->isMultivaluedFieldItemList($items)) {
      $processedMultiValuedItems = $this->viewMultivaluedFieldItemList($items);

      if (empty($processedMultiValuedItems)) {
        // No value set.
        $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
        $this->setProcessedFieldValue($fieldValue);
        return $fieldValue;
      }

      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, $processedMultiValuedItems, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    try {
      /* @var \Drupal\Core\Field\FieldItemInterface $item */
      $item = $items->first();
    }
    catch (MissingDataException $exception) {
      // No value set.
      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    if (empty($item)) {
      // No value set.
      $fieldValue = new FieldValueAndOptions($decoupledFieldKey, NULL, $decoupledFieldLocation);
      $this->setProcessedFieldValue($fieldValue);
      return $fieldValue;
    }

    $fieldValue = new FieldValueAndOptions($decoupledFieldKey, $this->viewFieldItem($item), $decoupledFieldLocation);
    $this->setProcessedFieldValue($fieldValue);
    return $fieldValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewMultivaluedFieldItemList(FieldItemListInterface $items) {
    if (!$items instanceof EntityReferenceFieldItemListInterface) {
      throw new UnexpectedFormatterException('Tried to render an entity field list, but the field does not implement EntityReferenceFieldItemListInterface.');
    }

    $data = [];

    foreach ($items as $delta => $item) {
      $data[$delta] = $this->viewFieldItem($item);
    }

    return $data;
  }

}
