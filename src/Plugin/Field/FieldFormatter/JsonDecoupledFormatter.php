<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;

/**
 * Plugin implementation of the "decoupled_json" formatter.
 *
 * @todo @see json_field module
 *
 * @FieldFormatter(
 *   id = "decoupled_json",
 *   label = @Translation("Json decoupled formatter"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class JsonDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    if ($this->isJson($item->getString())) {

      return Json::decode($item->getString());
    }
    else {
      throw new InvalidContentException($this->t("Invalid JSON string."));
    }
  }

  /**
   * Determine whether a string is valid JSON.
   *
   * @param string $string
   *   String to be checked.
   *
   * @return bool
   *   Returns TRUE if string is a valid JSON. Otherwise returns FALSE.
   */
  private function isJson($string) {
    return (is_string($string) && is_array(Json::decode($string)) && (json_last_error() == JSON_ERROR_NONE));
  }

}
