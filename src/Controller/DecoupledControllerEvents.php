<?php

namespace Drupal\decoupled_toolbox\Controller;

/**
 * Contains main events for decoupled controllers.
 */
abstract class DecoupledControllerEvents {

  public const EVENT__ALTER_QUERY = 'EVENT__ALTER_QUERY';
  public const EVENT__PROCESSABLE_CHECK = 'EVENT__PROCESSABLE_CHECK';

}
