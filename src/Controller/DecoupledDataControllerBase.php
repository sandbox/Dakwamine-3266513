<?php

namespace Drupal\decoupled_toolbox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Event\AlterQueryEvent;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\Event\ProcessableCheckEvent;
use Drupal\decoupled_toolbox\Service\DecoupledRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for entity decoupled data.
 */
abstract class DecoupledDataControllerBase extends ControllerBase {

  const EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT = 'decoupled_toolbox.controller.rendered_output.built';

  const SETTINGS__STATE__DEBUG_ENABLED = 'decoupled_toolbox.state.debug_enabled';

  /**
   * Decoupled renderer service.
   *
   * @var \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface
   */
  protected $decoupledRenderer;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * EntityDecoupledDataController constructor.
   *
   * @param \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface $decoupledRenderer
   *   The decoupled renderer service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   */
  public function __construct(
    DecoupledRendererInterface $decoupledRenderer,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher) {
    $this->decoupledRenderer = $decoupledRenderer;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Gives a last opportunity to alter the query before execution.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to alter.
   */
  protected function alterQuery(QueryInterface $query) {
    $this->eventDispatcher->dispatch(DecoupledControllerEvents::EVENT__ALTER_QUERY, new AlterQueryEvent($query));
  }

  /**
   * Additional checks if this controller must process the given request.
   *
   * This is used to let other modules perform additional checks, e.g.:
   *   - limit allowed parameters,
   *   - filter out unwanted requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object from controller.
   * @param string $type
   *   Entity type ID from controller.
   * @param string $bundle
   *   Bundle ID from controller.
   *
   * @return bool
   *   TRUE if processable, FALSE otherwise.
   */
  protected function checkIfProcessable(Request $request, $type, $bundle) {
    $processableCheckEvent = new ProcessableCheckEvent($request, $type, $bundle);
    $this->eventDispatcher->dispatch(
      DecoupledControllerEvents::EVENT__PROCESSABLE_CHECK,
      $processableCheckEvent);

    if (empty($processableCheckEvent->getAccessResult())) {
      // No answer means processable (no event subscriber set this).
      return TRUE;
    }

    if ($processableCheckEvent->getAccessResult()->isForbidden()) {
      // Do not process it.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @var DecoupledRendererInterface $decoupledRenderer */
    $decoupledRenderer = $container->get('decoupled.renderer');
    /* @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo */
    $entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    /* @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher */
    $eventDispatcher = $container->get('event_dispatcher');

    return new static(
      $decoupledRenderer,
      $entityTypeBundleInfo,
      $entityTypeManager,
      $eventDispatcher
    );
  }

  /**
   * This will check debug flags.
   *
   * @param \Exception $exception
   *   The exception to process.
   *
   * @return string|null
   *   The exception message, or NULL if debug mode is disabled.
   */
  protected function exceptionToResponseMessage(\Exception $exception) {
    if (Settings::get(static::SETTINGS__STATE__DEBUG_ENABLED) === TRUE) {
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    // TODO: temp debug to check if settings.local.php is loaded.
    // TODO: this part must be removed later.
    $allSettings = Settings::getAll();
    if (!isset($allSettings[static::SETTINGS__STATE__DEBUG_ENABLED])) {
      // The setting is not set. Log it and temporarily return the message.
      \Drupal::logger('decoupled_toolbox')->debug('Debug enabled setting not set.');
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    return NULL;
  }

  /**
   * Call this when the collection has gathered all items.
   *
   * @param array $renderedOutput
   *   Collection array, passed by reference for direct edit.
   */
  protected function onBaseEntityRenderBuilt(array &$renderedOutput) {
    // Let other handlers alter the output.
    $event = new OnRenderedOutputBuiltEvent($renderedOutput);
    $this->eventDispatcher->dispatch(static::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT, $event);

    // Replace the output with the altered one.
    $renderedOutput = $event->getRenderedOutput();
  }

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  protected function validateQueryParameterAsPositiveInteger($parameter) {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) !== FALSE;
  }

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  protected function validateQueryParameterAsZeroAndPositiveInteger($parameter) {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]]) !== FALSE;
  }

}
