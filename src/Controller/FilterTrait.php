<?php

namespace Drupal\decoupled_toolbox\Controller;

use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\decoupled_toolbox\Event\ConditionPreprocessEvent;
use Symfony\Component\HttpFoundation\Request;

/**
 * Used for controllers which need the filter mechanism.
 */
trait FilterTrait {

  use StringTranslationTrait;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Appends conditions on the entity query using filter query parameter.
   *
   * Filter parameters must use one of the following syntaxes:
   *   - filter[{filter_index}][f]={field_name}
   *   - filter[{filter_index}][v]={expected single value}
   *   - filter[{filter_index}][v][]={expected value of an array}
   *   - filter[{filter_index}][c]={entity query condition: IN, >, <, etc.}
   *
   * Example:
   *   - filter[0][f]=title
   *   - filter[0][v]=Title%20of%20the%20content
   *   - filter[1][f]=field_tag
   *   - filter[1][v][]=10
   *   - filter[1][v][]=1337
   *   - filter[1][c]=IN
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The symfony request.
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The entity query helper.
   * @param string $expectedEntityTypeId
   *   The expected entity type ID.
   * @param string $expectedEntityBundle
   *   The expected entity bundle.
   *
   * @throws \Drupal\Core\Entity\Query\QueryException
   */
  protected function appendConditionsFromQueryParameters(Request $request, QueryInterface $query, $expectedEntityTypeId, $expectedEntityBundle) {
    $queryParameters = $request->query->get('filter', []);

    foreach ($queryParameters as $queryParameter) {
      if (empty($queryParameter['f'])) {
        throw new QueryException($this->getStringTranslation()->translate('Invalid parameter "f".'));
      }

      if (empty($queryParameter['v'])) {
        throw new QueryException($this->getStringTranslation()->translate('Invalid parameter "v".'));
      }

      $this->getEventDispatcher()->dispatch(FilterInterface::EVENT__CONDITION_PREPROCESS, new ConditionPreprocessEvent($queryParameter, $expectedEntityTypeId, $expectedEntityBundle));

      if (empty($queryParameter['f']) || empty($queryParameter['v'])) {
        // The condition was canceled due to preprocessors.
        return;
      }

      $query->condition(
        $queryParameter['f'],
        $queryParameter['v'],
        empty($queryParameter['c']) ? NULL : $queryParameter['c']);
    }
  }

  /**
   * Gets the event dispatcher service.
   *
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The service.
   */
  protected function getEventDispatcher() {
    if (!$this->eventDispatcher) {
      $this->eventDispatcher = \Drupal::service('event_dispatcher');
    }

    return $this->eventDispatcher;
  }

}
