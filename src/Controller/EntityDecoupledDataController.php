<?php

namespace Drupal\decoupled_toolbox\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Service\EntityViewDisplayManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

/**
 * Controller for entity decoupled data.
 */
class EntityDecoupledDataController extends DecoupledDataControllerBase {

  use FilterTrait;

  /**
   * Called by the route decoupled_toolbox.entity_decoupled_data.collection.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   JSON response containing entities' values, or standard Response on
   *   errors.
   */
  public function collection(Request $request, $type, $bundle) {
    switch ($request->getMethod()) {
      case Request::METHOD_GET:
        try {
          return $this->getCollection($request, $type, $bundle);
        }
        catch (InvalidParameterException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_BAD_REQUEST);
        }
        catch (CouldNotRetrieveContentException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_NOT_FOUND);
        }
        catch (UnavailableDecoupledViewDisplayException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_NOT_IMPLEMENTED);
        }
        catch (QueryException $exception) {
          // Failure on query execution or preparation.
          return new Response(Settings::get(static::SETTINGS__STATE__DEBUG_ENABLED) ? $this->t('There was an error in the query: @message.', ['@message' => $exception->getMessage()]) : NULL, Response::HTTP_BAD_REQUEST);
        }
        catch (\Exception $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Should never happen, because methods are filtered on the routing side.
    throw new MethodNotAllowedException([Request::METHOD_GET]);
  }

  /**
   * Gets a collection of entity decoupled data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response containing the entity decoupled data.
   *
   * @throws \Drupal\Core\Entity\Query\QueryException
   *   Error on the query execution or preparation.
   * @throws \Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException
   *   Content not found or storage error.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   Not a valid content entity.
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   Entity type is not suitable for decoupled rendering.
   */
  protected function getCollection(Request $request, $type, $bundle) {
    $offset = $request->query->get('offset');
    if ($offset === NULL) {
      throw new InvalidParameterException($this->t('offset parameter is undefined.'));
    }
    if (!$this->validateQueryParameterAsZeroAndPositiveInteger($offset)) {
      throw new InvalidParameterException($this->t('Invalid offset parameter.'));
    }

    $limit = $request->query->get('limit');
    if ($limit === NULL) {
      throw new InvalidParameterException($this->t('limit parameter is undefined.'));
    }
    if (!$this->validateQueryParameterAsPositiveInteger($limit)) {
      throw new InvalidParameterException($this->t('Invalid limit parameter.'));
    }

    // Make sure the entity type and bundle exist.
    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($type);

    if (empty($bundleInfo[$bundle])) {
      throw new InvalidParameterException($this->t('Invalid entity type or bundle.'));
    }

    if (!$this->checkIfProcessable($request, $type, $bundle)) {
      // The requested data is not allowed.
      throw new InvalidParameterException($this->t('Not allowed to process the requested data.'));
    }

    // If bundle info was found, we assume the entity type exists.
    try {
      $entityStorage = $this->entityTypeManager()->getStorage($type);
    }
    catch (\Exception $exception) {
      // Something really wrong with the database occurred because valid
      // entities must have a storage. Do not process anymore.
      throw new CouldNotRetrieveContentException();
    }

    // Prepare the query.
    $query = $entityStorage->getQuery()
      ->range($offset, $limit);

    // Get the bundle key if it exists.
    try {
      $entityTypeDefinition = $this->entityTypeManager()->getDefinition($type);
    }
    catch (PluginNotFoundException $exception) {
      throw new InvalidParameterException($this->t('Invalid entity type.'));
    }

    $bundleKey = $entityTypeDefinition->getKey('bundle');

    if (!empty($bundleKey)) {
      // Not a bundleless entity.
      $query->condition($bundleKey, $bundle);
    }

    $this->appendConditionsFromQueryParameters($request, $query, $type, $bundle);
    $this->alterQuery($query);

    // May fail with a QueryException.
    $ids = $query->execute();

    $display = $request->query->get('display') ?? EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID;
    $rendered = [];
    foreach ($ids as $id) {
      // Use $cacheTags parameter if the response needs to be cached.
      $cacheTags = [];
      $fullRenderedEntity = $this->decoupledRenderer->renderByEntityTypeAndId($type, $id, $cacheTags,$display);

      // Let other modules / events perform last changes.
      $this->onBaseEntityRenderBuilt($fullRenderedEntity);

      $rendered[] = $fullRenderedEntity;
    }

    return new JsonResponse($rendered);
  }

}
