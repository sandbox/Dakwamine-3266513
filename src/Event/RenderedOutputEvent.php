<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Extends the base Event for when the renderer has finished rendering.
 */
class RenderedOutputEvent extends Event {

  /**
   * The rendered entity object.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $renderedEntity;

  /**
   * Array of the rendered entity object.
   *
   * @var array
   */
  protected $renderedOutput;

  /**
   * Array of cache tags.
   *
   * @var array
   */
  protected $cacheTags;

  /**
   * RenderedOutputEvent constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which was used by renderer.
   * @param array $renderedOutput
   *   The output from the renderer.
   * @param array $cacheTags
   *   The output from the renderer.
   */
  public function __construct(ContentEntityInterface $entity, array &$renderedOutput, array &$cacheTags) {
    $this->renderedEntity = $entity;
    $this->renderedOutput = &$renderedOutput;
    $this->cacheTags = &$cacheTags;
  }

  /**
   * Gets the entity which was rendered.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity object.
   */
  public function getRenderedEntity() {
    return $this->renderedEntity;
  }

  /**
   * Gets the rendered output.
   *
   * @return array
   *   The rendered output, passed by reference for direct edit.
   */
  public function &getRenderedOutput() {
    return $this->renderedOutput;
  }

  /**
   * Gets the cache tags of the rendered output.
   *
   * @return array
   *   The rendered output cache tags, passed by reference for direct edit.
   */
  public function &getCacheTags() {
    return $this->cacheTags;
  }

}
