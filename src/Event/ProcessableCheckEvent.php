<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Event for DecoupledControllerEventsInterface::EVENT__PROCESSABLE_CHECK.
 */
class ProcessableCheckEvent extends Event {

  /**
   * Bundle ID from controller.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The request object from controller.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Entity type from controller.
   *
   * @var string
   */
  protected $type;

  /**
   * The result of the access check.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $accessResult;

  /**
   * AlterQueryEvent constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object from controller.
   * @param string $type
   *   The entity type from controller.
   * @param string $bundle
   *   The bundle from controller.
   */
  public function __construct(Request $request, $type, $bundle) {
    $this->bundle = $bundle;
    $this->request = $request;
    $this->type = $type;
  }

  /**
   * Gets the access result.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function getAccessResult() {
    return $this->accessResult;
  }

  /**
   * Gets the bundle.
   *
   * @return string
   *   Bundle ID.
   */
  public function getBundle() {
    return $this->bundle;
  }

  /**
   * Gets the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Gets the entity type.
   *
   * @return string
   *   Entity type ID.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the access result.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $accessResult
   *   The access result to set.
   */
  public function setAccessResult(AccessResultInterface $accessResult) {
    $this->accessResult = $accessResult;
  }

}
