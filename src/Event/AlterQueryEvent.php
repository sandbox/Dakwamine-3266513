<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event to use during DecoupledControllerEventsInterface::EVENT__ALTER_QUERY.
 */
class AlterQueryEvent extends Event {

  /**
   * The query object to alter.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $query;

  /**
   * AlterQueryEvent constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to alter.
   */
  public function __construct(QueryInterface $query) {
    $this->query = $query;
  }

  /**
   * Gets the query object to alter.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query to alter.
   */
  public function getQuery() {
    return $this->query;
  }

}
