<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event to use during FilterInterface::EVENT__CONDITION_PREPROCESS.
 */
class ConditionPreprocessEvent extends Event {

  /**
   * An array containing condition info.
   *
   * This value structure is prepared in
   * FilterTrait::appendConditionsFromQueryParameters().
   *
   * @var array
   */
  protected $condition;

  /**
   * The expected entity bundle.
   *
   * @var string
   */
  protected $expectedEntityBundle;

  /**
   * The expected entity type ID.
   *
   * @var string
   */
  protected $expectedEntityTypeId;

  /**
   * ConditionPreprocessEvent constructor.
   *
   * @param array $condition
   *   An array containing info as prepared in
   *   FilterTrait::appendConditionsFromQueryParameters(). Passed by reference.
   * @param string $expectedEntityTypeId
   *   The expected entity type ID.
   * @param string $expectedEntityBundle
   *   The expected entity bundle.
   */
  public function __construct(array &$condition, $expectedEntityTypeId, $expectedEntityBundle) {
    $this->condition = &$condition;
    $this->expectedEntityTypeId = $expectedEntityTypeId;
    $this->expectedEntityBundle = $expectedEntityBundle;
  }

  /**
   * Gets the condition array.
   *
   * This is passed by reference for easy handling.
   *
   * @return array
   *   The condition array ready for preprocess.
   */
  public function &getCondition() {
    return $this->condition;
  }

  /**
   * Gets the expected entity bundle.
   *
   * @return string
   *   The entity bundle.
   */
  public function getExpectedEntityBundle() {
    return $this->expectedEntityBundle;
  }

  /**
   * Gets the expected entity type ID.
   *
   * @return string
   *   The entity type ID.
   */
  public function getExpectedEntityTypeId() {
    return $this->expectedEntityTypeId;
  }

}
