<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Extends the base Event for when the rendered output is ready for alteration.
 */
class OnRenderedOutputBuiltEvent extends Event {

  /**
   * Array of the output.
   *
   * @var array
   */
  protected $renderedOutput;

  /**
   * OnOutputBuiltEvent constructor.
   *
   * @param array $renderedOutput
   *   The rendered output to alter.
   */
  public function __construct(array &$renderedOutput) {
    $this->renderedOutput = &$renderedOutput;
  }

  /**
   * Gets the output.
   *
   * @return array
   *   The rendered output, passed by reference for direct edit.
   */
  public function &getRenderedOutput() {
    return $this->renderedOutput;
  }

  /**
   * Replaces the rendered output.
   *
   * @param array $newRenderedOutput
   *   The new rendered output; replaces the old referenced array.
   */
  public function setRenderedOutput(array &$newRenderedOutput) {
    $this->renderedOutput = &$newRenderedOutput;
  }

}
