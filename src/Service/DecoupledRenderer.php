<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Event\RenderedOutputEvent;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\FieldNotYetProcessedException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\DecoupledFormatterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Renders an entity for decoupled.
 */
class DecoupledRenderer implements DecoupledRendererInterface {

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * Array containing entities which are currently rendering.
   *
   * Use this to prevent entity reference infinite loops.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  private $currentEntityRenderStack = [];

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Contains a collection of EntityViewDisplayInterface for reuse.
   *
   * Items are indexed by ID such as "node.article.teaser".
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface[]
   */
  private $entityViewDisplayCache = [];

  /**
   * Entity view display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $entityViewDisplayStorage;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * A flag telling if we want to attempt to retrieve the entity from cache.
   *
   * Set to TRUE if we want to, FALSE otherwise.
   *
   * @var bool
   */
  private $useCachedEntity;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    CacheBackendInterface $cacheBackend,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher,
    LanguageManagerInterface $languageManager) {
    $this->cacheBackend = $cacheBackend;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->languageManager = $languageManager;
    $this->entityViewDisplayStorage = $entityTypeManager->getStorage('entity_view_display');

    $this->useCachedEntity = (bool)Settings::get(static::SETTINGS__CACHE__ENABLED, TRUE);
  }

  /**
   * Adds the specified entity to the current entity render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to add.
   * @param string $viewMode
   *   The entity view mode.
   */
  private function addEntityToCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode) {
    $this->currentEntityRenderStack[$this->getEntityRenderStackEntityId($entity, $viewMode)] = $entity;
  }

  /**
   * Caches the render result.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which was rendered.
   * @param string $viewMode
   *   The entity view mode.
   * @param array $finalizedRender
   *   Array of values ready for decoupled output.
   * @param array $cacheTagsToMerge
   *   Array of additional cache tags to merge.
   */
  private function cacheRenderedEntityResult(ContentEntityInterface $entity, $viewMode, array $finalizedRender, array $cacheTagsToMerge) {
    $mergedCacheTags = Cache::mergeTags($cacheTagsToMerge, $entity->getCacheTags());
    $cacheId = $this->getCacheIdForEntityAndViewMode($entity, $viewMode);
    $this->cacheBackend->set($cacheId, $finalizedRender, Cache::PERMANENT, $mergedCacheTags);
  }

  /**
   * Finalizes the fields to render.
   *
   * The output is freed from metadata and ready for serialization such as a
   * JSON object.
   *
   * @param array $fieldsToRender
   *   Fields to render as given by $this->renderEntity(...).
   *
   * @return array
   *   True values without metadata ready for serialization.
   */
  private function finalizeFieldsToRender(array $fieldsToRender) {
    $finalized = [];

    foreach ($fieldsToRender as $item) {
      // $item['renderedFieldValue'] may be an associative array of a
      // FieldValueAndOptions object.
      $finalized[] = $item['renderedFieldValue'];
    }

    return $finalized;
  }

  /**
   * Gets the cache ID for the specified entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Cache ID ready for use.
   */
  private function getCacheIdForEntityAndViewMode(ContentEntityInterface $entity, $viewMode) {
    return $this->getCacheIdForEntityTypeAndIdAndViewMode($entity->getEntityTypeId(), $entity->id(), $viewMode);
  }

  /**
   * Gets the cache ID for the specified entity.
   *
   * @param string $entityTypeId
   *   Entity type ID.
   * @param int $id
   *   ID of the entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Cache ID ready for use.
   */
  private function getCacheIdForEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode) {
    return 'decoupled.' . $entityTypeId . '.' . $id . '.' . $viewMode;
  }

  /**
   * Retrieves the rendered entity cached data.
   *
   * @param string $entityTypeId
   *   Entity type ID.
   * @param int $id
   *   ID of the entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return object|null
   *   Cached data object, or NULL if not yet cached.
   *   This is a stdClass object containing cache values.
   */
  private function getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode) {
    $cacheId = $this->getCacheIdForEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode);

    $cacheObject = $this->cacheBackend->get($cacheId);

    if (empty($cacheObject)) {
      return NULL;
    }

    return $cacheObject;
  }

  /**
   * Gets the ID used for the currentEntityRenderStack array.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which we want to get the entity render stack ID.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Entity render stack ID.
   */
  private function getEntityRenderStackEntityId(ContentEntityInterface $entity, $viewMode) {
    return $entity->getEntityTypeId() . '.' . $entity->id() . '.' . $viewMode;
  }

  /**
   * Gets the entity view display for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the view display.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface|null
   *   The entity view display object, or NULL if not found.
   */
  private function getViewDisplayForEntity(ContentEntityInterface $entity, $viewMode) {
    $entityTypeId = $entity->getEntityTypeId();
    $entityBundle = $entity->bundle();

    $viewDisplayId = $entityTypeId . '.' . $entityBundle . '.' . $viewMode;

    if (isset($this->entityViewDisplayCache[$viewDisplayId])) {
      // Already loaded. Reuse the value. May return NULL if the entity view
      // display does not exist.
      return $this->entityViewDisplayCache[$viewDisplayId];
    }

    /* @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $viewDisplay */
    $viewDisplay = $this->entityViewDisplayStorage->load($viewDisplayId);

    // Cache the result.
    $this->entityViewDisplayCache[$viewDisplayId] = $viewDisplay;

    return $viewDisplay;
  }

  /**
   * Tells whether the given entity is already in the current render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return bool
   *   TRUE if already in current render stack. FALSE otherwise.
   */
  private function isEntityInCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode) {
    return array_key_exists($this->getEntityRenderStackEntityId($entity, $viewMode), $this->currentEntityRenderStack);
  }

  /**
   * Adds the specified entity to the current entity render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to remove.
   * @param string $viewMode
   *   The entity view mode.
   */
  private function removeEntityFromCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode) {
    unset($this->currentEntityRenderStack[$this->getEntityRenderStackEntityId($entity, $viewMode)]);
  }

  /**
   * {@inheritdoc}
   */
  public function renderByEntityTypeAndId($entityTypeId, $id, array &$cacheTags,$view_mode=EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID) {
    // Use the default view mode.
    if ($this->useCachedEntity) {
      // Attempt to load the entity from cache.
      $cachedDataObject = $this->getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $view_mode);

      if (!empty($cachedDataObject)) {
        // Entry found in cache.
        $cacheTags = $cachedDataObject->tags;

        // The returned value can be an empty array and be a valid value.
        return $cachedDataObject->data;
      }
    }

    try {
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    }
    catch (\Exception $exception) {
      // Storage error.
      throw new CouldNotRetrieveContentException();
    }

    $entity = $entityStorage->load($id);

    if (empty($entity)) {
      throw new CouldNotRetrieveContentException();
    }

    if (!$entity instanceof ContentEntityInterface) {
      throw new InvalidContentException();
    }

    // Render the entity. This will put the entity in cache upon completion.
    $rendered = $this->renderEntity($entity, $view_mode);

    // Retrieve the entity cached data created by $this->renderEntity(...).
    $cacheDataObject = $this->getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $view_mode);

    $cacheTags = [];

    if (!empty($cacheDataObject)) {
      // It happens that the currently rendered entity is already rendering in
      // the render stack without being yet in cache, especially when an entity
      // ends up referencing itself (through a field, terms, paragraphs, etc)
      // and the entity being new or after a cache clear.
      //
      // In this particular case here, we know that the entity has already been
      // put in cache so we can return the cache tags which were set.
      //
      // If it was not, we simply have to not return cache tags because they
      // will be handled by the entity which is rendering on the lower end of
      // the stack. After all, these are the same object.
      $cacheTags = $this->getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $view_mode)->tags;
    }

    return $rendered;
  }

  /**
   * Renders the specified content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to render.
   * @param string $viewMode
   *   View mode to use. Defaults to
   *   EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID.
   *
   * @return array
   *   The rendered content as array, ready for serialization.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   The entity has no decoupled view display set.
   */
  private function renderEntity(ContentEntityInterface $entity, $viewMode = EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID) {
    if ($this->isEntityInCurrentEntityRenderStack($entity, $viewMode)) {
      // The entity is already rendering in the stack. We have to prevent
      // infinite loops by not attempting to render this entity again.
      return [
        $entity->getEntityType()->getKey('id') => $entity->id(),
        'infiniteInclusionPrevention' => TRUE,
      ];
    }

    $this->addEntityToCurrentEntityRenderStack($entity, $viewMode);

    // Get the decoupled view mode for the given entity.
    $viewDisplay = $this->getViewDisplayForEntity($entity, $viewMode);

    if (empty($viewDisplay)) {
      throw new UnavailableDecoupledViewDisplayException();
    }

    // Get active fields on this view display.
    $enabledFields = $viewDisplay->getComponents();

    // Get the fields and their weight.
    $langCode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    $fieldsToRender = [];
    /** @var  $view_mode \Drupal\Core\Entity\Display\EntityViewDisplayInterface */
    $view_mode = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load($entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $viewMode);
    $cacheTagsFromFields = $view_mode->getCacheTags();
    foreach ($enabledFields as $fieldId => $fieldSettings) {
      $fieldRenderer = $viewDisplay->getRenderer($fieldId);

      if (!$fieldRenderer instanceof DecoupledFormatterInterface) {
        // Filter out fields without a valid formatter.
        continue;
      }

      $fieldItemList = $entity->get($fieldId);

      try {
        $renderedFieldValue = $fieldRenderer->viewElementsForDecoupled($fieldItemList, $langCode);

        // Retrieve cache tags from the processed field. This is useful for
        // entity references or specific formatters which may need to invalidate
        // caches.
        $cacheTagsFromFields = Cache::mergeTags($cacheTagsFromFields, $fieldRenderer->getProcessedFieldCacheTags());
      }
      catch (FieldNotYetProcessedException $exception) {
        // This should never happen here.
        continue;
      }
      catch (InvalidContentException $exception) {
        // Do not block on content error.
        // Silently ignore this field.
        continue;
      }
      catch (InvalidFormatterSettingsException $exception) {
        // Reporting the error in watchdog may result in database spam.
        // Silently ignore this field.
        continue;
      }
      catch (UnexpectedFormatterException $exception) {
        // A formatter was set on a field which does not support it. E.g. an
        // entity formatter was set on a field which does not implements
        // EntityReferenceFieldItemListInterface.
        // Silently ignore this field.
        continue;
      }

      $fieldsToRender[] = [
        'renderedFieldValue' => $renderedFieldValue,
        'weight' => $fieldSettings['weight'],
      ];
    }

    // Respect the weight set on the field UI of the view mode.
    $this->sortFieldsToRenderByWeight($fieldsToRender);

    // Remove transitive metadata (which serve only for the first stage of
    // the output building (static representation building)).
    $finalValues = $this->finalizeFieldsToRender($fieldsToRender);

    // Let other handlers alter the output.
    $event = new RenderedOutputEvent($entity, $finalValues, $cacheTagsFromFields);
    $this->eventDispatcher->dispatch(static::EVENT__RENDERER__OUTPUT__RENDERED__PREFIX . $entity->getEntityTypeId(), $event);

    // Cache the final values using the field tags.
    $this->cacheRenderedEntityResult($entity, $viewMode, $finalValues, $cacheTagsFromFields);

    // We are safe from infinite loops.
    $this->removeEntityFromCurrentEntityRenderStack($entity, $viewMode);

    return $finalValues;
  }

  /**
   * Sorts the fields to render by their weight key.
   *
   * @param array &$fieldsToRender
   *   Array of fields to render.
   */
  private function sortFieldsToRenderByWeight(array &$fieldsToRender) {
    usort($fieldsToRender, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
  }

}
