<?php

namespace Drupal\decoupled_toolbox\Service;

/**
 * Renders for decoupled.
 */
interface DecoupledRendererInterface {

  /**
   * Event ID prefix for output rendered event.
   *
   * Must be concatenated with entity type ID. E.g.:
   *
   * @code
   * DecoupledRendererInterface::EVENT__RENDERER__OUTPUT__RENDERED__PREFIX .
   *   'node'.
   * @endcode
   */
  const EVENT__RENDERER__OUTPUT__RENDERED__PREFIX = 'decoupled_toolbox.renderer.output.rendered.';

  /**
   * Settings.
   */
  const SETTINGS__CACHE__ENABLED = 'decoupled_toolbox.renderer.cache.enabled';

  /**
   * Renders an entity given the type and ID.
   *
   * Uses the default view mode:
   * EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID.
   *
   * @param string $entityTypeId
   *   Entity type ID.
   * @param int $id
   *   Entity ID.
   * @param array $cacheTags
   *   Cache tags associated to the rendered entity and its descendants from
   *   fields.
   *
   * @return array
   *   The rendered content as array, ready for serialization.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException
   *   When the content entity could not be retrieved, or the entity storage
   *   was
   *   unresponsive.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   Entity is not from an authorized content entity.
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   The decoupled view display is not available on the given entity.
   */
  public function renderByEntityTypeAndId($entityTypeId, $id, array &$cacheTags, $display = EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID);

}
