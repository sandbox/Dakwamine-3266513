<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Component\Utility\NestedArray;
use Drupal\decoupled_toolbox\Controller\DecoupledDataControllerBase;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\FieldValueAndOptions;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Standard field location solver.
 */
class LocationSolver implements LocationSolverInterface, EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[DecoupledDataControllerBase::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT] = ['onRenderedOutputBuilt'];
    return $events;
  }

  /**
   * Tells whether an array is sequential or not.
   *
   * This helps to not create useless indices in sequential arrays. Currently
   * not in use. Remove if really not needed.
   *
   * @param array $array
   *   The array.
   *
   * @return bool
   *   TRUE if sequential, FALSE otherwise.
   */
  private function isSequentialArray(array $array) {
    return array_keys($array) === range(0, count($array) - 1);
  }

  /**
   * Reacts on rendered output built.
   *
   * @param \Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent $event
   *   Event object.
   */
  public function onRenderedOutputBuilt(OnRenderedOutputBuiltEvent $event) {
    $outputResult = [];
    $this->solve($event->getRenderedOutput(), $outputResult);

    // Replace the rendered output.
    $event->setRenderedOutput($outputResult);
  }

  /**
   * Recursive lookup for array values.
   *
   * @param array $outputSourceExtract
   *   Extract from the output source array on which we can work.
   * @param array $outputResult
   *   The final output result array on which we can work directly (useful for
   *   absolute positioning of values).
   *
   * @return array
   *   The results of this lookup (contains children).
   */
  private function recursiveLookupArray(array $outputSourceExtract, array &$outputResult) {
    $localDescendantsResult = [];

    // Read each item.
    foreach ($outputSourceExtract as $key => $sub) {
      if (is_array($sub)) {
        // Recurse further.
        $localDescendantsResult[$key] = $this->recursiveLookupArray($sub, $outputResult);
        continue;
      }
      elseif ($sub instanceof FieldValueAndOptions) {
        $fvoResult = $this->recursiveLookupFieldValueAndOptions($sub, $outputResult);

        if (is_null($fvoResult)) {
          // The field has been moved using absolute method.
          // No need to process further.
          continue;
        }

        // When building a (sub)array with FieldValueAndOptions, it likely means
        // we are building the root output of an entity, because it is in the
        // display form that we set properties in the formatters which generate
        // such objects to process. In other words, FieldValueAndOptions objects
        // and other value types should never be mixed up in a same array.
        // At this stage, the parent array has already been correctly ordered by
        // weight.
        // Thus it is safe to operate by setting directly the key.
        $localDescendantsResult[$fvoResult['key']] = $fvoResult['value'];
        continue;
      }

      // This is likely a value which contains scalar data.
      $localDescendantsResult[$key] = $sub;
    }

    return $localDescendantsResult;
  }

  /**
   * Call this when starting solving.
   *
   * @param mixed $outputSource
   *   The output source reference. Should not be modified. May be an array
   *   (expected usual case) or a FieldValueOptions object, or a scalar (really
   *   unexpected because it means the output is a single value not managed by
   *   FieldValueOptions container).
   * @param array $outputResult
   *   The output source result.
   */
  private function recursiveLookupEntryPoint(&$outputSource, array &$outputResult) {
    if (is_array($outputSource)) {
      $localDescendantsResult = $this->recursiveLookupArray($outputSource, $outputResult);
    }
    elseif ($outputSource instanceof FieldValueAndOptions) {
      $localDescendantsResult = $this->recursiveLookupFieldValueAndOptions($outputSource, $outputResult);
    }
    else {
      // Unlikely to happen, but meh.
      $localDescendantsResult = $outputSource;
    }

    // Finalize the merge with the $outputResult array.
    // NestedArray::mergeDeepArray(...) is absolutely necessary to correctly
    // merge the arrays as expected (+ operator and array_merge(...) do not
    // work).
    $outputResult = NestedArray::mergeDeepArray([$outputResult, $localDescendantsResult], TRUE);
  }

  /**
   * Recursive lookup for a single FieldValueAndOptions value.
   *
   * @param \Drupal\decoupled_toolbox\FieldValueAndOptions $outputSourceExtract
   *   Extract from the output source array on which we can work.
   * @param array $outputResult
   *   The final output result array on which we can work directly (useful for
   *   absolute positioning of values).
   *
   * @return array
   *   The results of this lookup (contains children).
   */
  private function recursiveLookupFieldValueAndOptions(FieldValueAndOptions $outputSourceExtract, array &$outputResult) {
    $itemKey = $outputSourceExtract->getDecoupledFieldKey();
    $itemValue = $outputSourceExtract->getValue();
    $itemLocation = $outputSourceExtract->getDecoupledFieldLocation();

    if (is_array($itemValue)) {
      $localDescendantsResult = $this->recursiveLookupArray($itemValue, $outputResult);
    }
    elseif ($itemValue instanceof FieldValueAndOptions) {
      $localDescendantsResult = $this->recursiveLookupFieldValueAndOptions($itemValue, $outputResult);
    }
    else {
      // Scalar values (integer, strings, etc).
      $localDescendantsResult = $itemValue;
    }

    if (!empty($itemLocation)) {
      // A location has been defined.
      // Break the $itemLocation.
      $hierarchy = explode('/', $itemLocation);
      $absolutePath = $hierarchy[0] === '';

      if ($absolutePath) {
        // Work directly on the finalResult array.
        // Reverse the hierarchy to reverse build the current array.
        $reverseHierarchy = array_reverse($hierarchy);

        // Remove the last item, which is an empty string (because it is an
        // absolute hierarchy).
        array_pop($reverseHierarchy);

        // Start by setting the value (which may be an array or a scalar).
        $array = [$itemKey => $localDescendantsResult];

        foreach ($reverseHierarchy as $step) {
          if ($step === '') {
            // Empty step means: "insert with no key nor delta, aka append".
            $array = [$array];
            continue;
          }

          $array = [$step => $array];
        }

        // Finally merge the arrays.
        // NestedArray::mergeDeepArray() is necessary (array_merge(...) nor +
        // operator not working).
        $outputResult = NestedArray::mergeDeepArray([$outputResult, $array], TRUE);

        // Return NULL to tell caller that this item is already placed.
        return NULL;
      }

      // TODO: relative handling may be done here.
    }

    // Simply locate the value as usual.
    return [
      'key' => $itemKey,
      'value' => $localDescendantsResult,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function solve(array &$outputSource, array &$outputResult) {
    // Solving must be done on a new array.
    $this->recursiveLookupEntryPoint($outputSource, $outputResult);
  }

}
