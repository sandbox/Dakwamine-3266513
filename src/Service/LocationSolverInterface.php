<?php

namespace Drupal\decoupled_toolbox\Service;

/**
 * Inheriting classes may solve fields location in the decoupled output.
 */
interface LocationSolverInterface {

  /**
   * Solves filed location.
   *
   * @param array &$outputSource
   *   The output source array. Passed by reference.
   * @param array &$outputResult
   *   The output result array built by the solver. Passed by reference.
   */
  public function solve(array &$outputSource, array &$outputResult);

}
