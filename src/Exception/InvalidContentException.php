<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * Not an authorized content entity for processing.
 */
class InvalidContentException extends \Exception {}
