<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * Tried to work on field values, but they were not processed yet.
 */
class FieldNotYetProcessedException extends \Exception {}
