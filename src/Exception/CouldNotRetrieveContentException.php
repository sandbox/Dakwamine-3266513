<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * Tried to load a content entity but was not found or an error occurred.
 */
class CouldNotRetrieveContentException extends \Exception {}
