<?php

namespace Drupal\decoupled_toolbox_decoupled_router\EventSubscriber;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\decoupled_router\Controller\PathTranslator;
use Drupal\decoupled_toolbox\Controller\FilterInterface;
use Drupal\decoupled_toolbox\Event\ConditionPreprocessEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Alias condition preprocessor using decoupled router.
 */
class AliasConditionPreprocessor implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A path translator controller instance.
   *
   * Contains the method to translate the path to an entity.
   *
   * @var \Drupal\decoupled_router\Controller\PathTranslator
   */
  protected $pathTranslatorController;

  /**
   * Constructs a new AliasConditionPreprocessor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel
   *   HTTP Kernel service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher,
    HttpKernelInterface $httpKernel) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathTranslatorController = new PathTranslator($eventDispatcher, $httpKernel);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[FilterInterface::EVENT__CONDITION_PREPROCESS] = ['onConditionPreprocess'];

    return $events;
  }

  /**
   * Called when a condition is preprocessed.
   *
   * @param \Drupal\decoupled_toolbox\Event\ConditionPreprocessEvent $event
   *   The dispatched event.
   *
   * @throws \Exception
   *   A serious error occurred.
   */
  public function onConditionPreprocess(ConditionPreprocessEvent $event) {
    $condition = &$event->getCondition();

    if ($condition['f'] !== 'path.alias') {
      // Not a "field" we want to manage.
      return;
    }

    $expectedEntityTypeId = $event->getExpectedEntityTypeId();
    $expectedEntityBundle = $event->getExpectedEntityBundle();

    // Retrieve the "id" field name from entity type definition.
    try {
      $entityTypeDefinition = $this->entityTypeManager->getDefinition($expectedEntityTypeId);
    }
    catch (PluginNotFoundException $exception) {
      // Not an exception we can manage.
      throw new \Exception();
    }

    $idKey = $entityTypeDefinition->getKey('id');

    // Replace the field name.
    $condition['f'] = $idKey;

    // Attempt to translate the path.
    if (is_array($condition['v'])) {
      foreach ($condition['v'] as $delta => $value) {
        $entityId = $this->translateAliasToEntityId($value, $expectedEntityTypeId, $expectedEntityBundle);

        if (empty($entityId)) {
          // Leave the original value.
          continue;
        }

        // Replace the value.
        $condition['v'][$delta] = $entityId;
      }

      return;
    }

    // Process a single value.
    $entityId = $this->translateAliasToEntityId($condition['v'], $expectedEntityTypeId, $expectedEntityBundle);

    if (empty($entityId)) {
      // Leave the original value.
      return;
    }

    // Replace the value.
    $condition['v'] = $entityId;
  }

  /**
   * Translates an alias to the original entity.
   *
   * @param string $alias
   *   Alias to translate.
   * @param string $expectedEntityType
   *   Use to filter out the entity if it does not match.
   * @param string $expectedEntityBundle
   *   Use to filter out the entity if it does not match.
   *
   * @return int|null
   *   Returns the entity ID which matches the alias, or NULL if the entity is
   *   not of expected type or bundle or if the entity does not exist.
   */
  private function translateAliasToEntityId($alias, $expectedEntityType, $expectedEntityBundle) {
    $fakeRequest = new Request(['path' => $alias]);
    try {
      $fakeJsonResponse = $this->pathTranslatorController->translate($fakeRequest);
    }
    catch (ParamNotConvertedException $exception) {
      // Undeclared exception fired by ParamConverterManager::convert().
      // It means that the path links to nothing even though it has a valid
      // route (e.g. trying to resolve a /node/{node} but not having an existing
      // entity at this path).
      // Silently ignore by not resolving this path.
      return NULL;
    }

    // Extract the entity info from the fake JSON response.
    $json = Json::decode($fakeJsonResponse->getContent());

    if (empty($json['entity']['type'])) {
      // Not found.
      return NULL;
    }

    if ($json['entity']['type'] === $expectedEntityType && $json['entity']['bundle'] === $expectedEntityBundle) {
      return $json['entity']['id'];
    }

    // Not an entity with expected type or bundle.
    return NULL;
  }

}
