<?php

namespace Drupal\decoupled_toolbox_duration_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\GenericDecoupledFormatter;
use Drupal\duration_field\Plugin\Field\FieldType\DurationField;

/**
 * Plugin implementation of the 'decoupled_duration_field' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_duration_field",
 *   label = @Translation("Duration field decoupled formatter"),
 *   field_types = {
 *     "duration",
 *   }
 * )
 */
class DurationFieldDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    if (!$item instanceof DurationField) {
      throw new InvalidContentException($this->t('Not a valid duration field item.'));
    }

    try {
      if ($item->isEmpty()) {
        return NULL;
      }

      /* @var \Drupal\duration_field\Plugin\DataType\DateIntervalInterface $duration*/
      $duration = $item->get('duration');

      return $duration->getValue();
    }
    catch (\Exception $exception) {
      throw new InvalidContentException($this->t('Duration value could not be retrieved.'));
    }
  }

}
