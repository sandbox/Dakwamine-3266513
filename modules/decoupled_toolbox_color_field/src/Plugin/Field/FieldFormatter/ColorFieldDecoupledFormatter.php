<?php

namespace Drupal\decoupled_toolbox_color_field\Plugin\Field\FieldFormatter;

use Drupal\color_field\ColorHex;
use Drupal\color_field\Plugin\Field\FieldType\ColorFieldType;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\GenericDecoupledFormatter;

/**
 * Plugin implementation of the 'decoupled_color_field' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_color_field",
 *   label = @Translation("Color field decoupled formatter"),
 *   field_types = {
 *     "color_field_type",
 *   }
 * )
 */
class ColorFieldDecoupledFormatter extends GenericDecoupledFormatter {

  const SETTINGS__RGBA_OUTPUT = 'rgba_output';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::SETTINGS__RGBA_OUTPUT => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * Gets the settings summary array.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Array of markup.
   */
  protected function getSettingsSummary() {
    return array_merge(
      parent::getSettingsSummary(),
      [
        $this->t('Use RGB(A) output: <strong>@useRGBA</strong>', ['@useRGBA' => $this->useRGBA() ? $this->t('Yes') : $this->t('No')]),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState) {
    $form = parent::settingsForm($form, $formState);

    $form[static::SETTINGS__RGBA_OUTPUT] = [
      '#default_value' => $this->useRgba(),
      '#description' => $this->t('If checked, rgb(a) output will be used. Otherwise, hexadecimal output will be used.'),
      '#title' => $this->t('Use RGB(A) output'),
      '#type' => 'checkbox',
      '#weight' => -10,
    ];

    return $form;
  }

  /**
   * Tells if this formatter uses RGB(A) notation for output.
   *
   * @return bool
   *   TRUE if using rgb(a), FALSE otherwise.
   */
  private function useRgba() {
    return !empty($this->getSetting(static::SETTINGS__RGBA_OUTPUT));
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item) {
    if (!$item instanceof ColorFieldType) {
      throw new InvalidContentException($this->t('Not a valid color field item.'));
    }

    try {
      $colorHex = new ColorHex($item->color, $item->opacity);

      $opacity = $this->getFieldSetting('opacity');

      if (!$this->useRGBA()) {
        // Hex output.
        if ($opacity) {
          return mb_strtolower($colorHex->toString(TRUE));
        }

        return mb_strtolower($colorHex->toString(FALSE));
      }

      // RGB(A) output.
      if ($opacity) {
        return mb_strtolower($colorHex->toRgb()->toString(TRUE));
      }

      return mb_strtolower($colorHex->toRgb()->toString(FALSE));
    }
    catch (\Exception $exception) {
      throw new InvalidContentException($this->t('Color value could not be retrieved.'));
    }
  }

}
