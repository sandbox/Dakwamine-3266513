<?php

namespace Drupal\decoupled_toolbox_group\Controller;

use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Controller\DecoupledDataControllerBase;
use Drupal\decoupled_toolbox\Controller\FilterTrait;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Service\EntityViewDisplayManagerInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

/**
 * Controller for entity decoupled data per group.
 */
class GroupEntityDecoupledDataController extends DecoupledDataControllerBase {

  use FilterTrait;

  /**
   * Group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $groupContentStorage;

  /**
   * Called by route decoupled_toolbox.group.entity_decoupled_data.collection.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $gid
   *   Group ID.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   JSON response containing entities' values, or standard Response on
   *   errors.
   */
  public function collection(Request $request, $gid, $type, $bundle) {
    switch ($request->getMethod()) {
      case Request::METHOD_GET:
        try {
          return $this->getCollection($request, $gid, $type, $bundle);
        }
        catch (InvalidParameterException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_BAD_REQUEST);
        }
        catch (CouldNotRetrieveContentException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_NOT_FOUND);
        }
        catch (UnavailableDecoupledViewDisplayException $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_NOT_IMPLEMENTED);
        }
        catch (QueryException $exception) {
          // Failure on query execution or preparation.
          return new Response(Settings::get(static::SETTINGS__STATE__DEBUG_ENABLED) ? $this->t('There was an error in the query: @message.', ['@message' => $exception->getMessage()]) : NULL, Response::HTTP_BAD_REQUEST);
        }
        catch (\Exception $exception) {
          return new Response($this->exceptionToResponseMessage($exception), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Should never happen, because methods are filtered on the routing side.
    throw new MethodNotAllowedException([Request::METHOD_GET]);
  }

  /**
   * Filters the real entity IDs against query parameters filters.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request which initiated the current request.
   * @param array $entityIds
   *   The entity IDs to filter out.
   * @param string $expectedType
   *   Needed to get the Query object on the entity tables.
   * @param string $expectedBundle
   *   The expected bundle. Set the same value as $expectedType for bundleless
   *   entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\Query\QueryException
   *
   * @return int[]
   *   An array of entity IDs filtered by the query parameters.
   */
  private function filterContentByQueryParameters(Request $request, array $entityIds, $expectedType, $expectedBundle) {
    if (empty($entityIds)) {
      // Nothing to filter.
      return [];
    }

    $entityTypeDefinition = $this->entityTypeManager()->getDefinition($expectedType);
    $idKey = $entityTypeDefinition->getKey('id');

    $query = $this->entityTypeManager()
      ->getStorage($expectedType)->getQuery()
      ->condition($idKey, $entityIds, 'IN');

    $this->appendConditionsFromQueryParameters($request, $query, $expectedType, $expectedBundle);
    $this->alterQuery($query);

    // May trigger QueryException on failure.
    return $query->execute();
  }

  /**
   * Gets a collection of entity decoupled data per group.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $gid
   *   Group ID.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON response containing the entity decoupled data.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException
   *   Content not found or storage error.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   Not a valid content entity.
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   Entity type is not suitable for decoupled rendering.
   * @throws \Exception
   *   Miscellaneous errors.
   */
  protected function getCollection(Request $request, $gid, $type, $bundle) {
    if (!$this->validateQueryParameterAsPositiveInteger($gid)) {
      throw new InvalidParameterException($this->t('Invalid group ID parameter.'));
    }

    $offset = $request->query->get('offset');
    if ($offset === NULL) {
      throw new InvalidParameterException($this->t('offset parameter is undefined.'));
    }
    if (!$this->validateQueryParameterAsZeroAndPositiveInteger($offset)) {
      throw new InvalidParameterException('Invalid offset parameter.');
    }

    $limit = $request->query->get('limit');
    if ($limit === NULL) {
      throw new InvalidParameterException($this->t('limit parameter is undefined.'));
    }
    if (!$this->validateQueryParameterAsPositiveInteger($limit)) {
      throw new InvalidParameterException($this->t('Invalid limit parameter.'));
    }

    // Make sure the group exists.
    try {
      $groupStorage = $this->entityTypeManager()->getStorage('group');
    }
    catch (\Exception $exception) {
      // Group is not installed. Should never happen because of dependency on
      // group module.
      throw new \Exception($this->t('Group is not installed.'));
    }

    /* @var \Drupal\group\Entity\GroupInterface $group */
    $group = $groupStorage->load($gid);

    if (empty($group)) {
      throw new \Exception($this->t('Group with id @id does not exist.', ['@id' => $gid]));
    }

    if (!$this->checkIfProcessable($request, $type, $bundle)) {
      // The requested data is not allowed.
      throw new InvalidParameterException($this->t('Not allowed to process the requested data.'));
    }

    // Let's retrieve entity IDs.
    $groupContentIds = $this->getGroupContentIdsPerGroup($group, $type, $bundle, $offset, $limit);

    $groupContentStorage = $this->getGroupContentStorage();
    $batchSize = 50;
    $entityIds_unfiltered = [];

    $groupContentIds_count = count($groupContentIds);

    for ($i = 0; $i < $groupContentIds_count; $i += $batchSize) {
      $groupContentIds_subset = array_slice($groupContentIds, $i, $batchSize, TRUE);

      /* @var \Drupal\group\Entity\GroupContentInterface[] $groupContentEntities */
      $groupContentEntities = $groupContentStorage->loadMultiple($groupContentIds_subset);

      foreach ($groupContentEntities as $groupContentEntity) {
        // Get the original entity linked to this GroupContent instance.
        $originalEntity = $groupContentEntity->getEntity();
        $entityIds_unfiltered[$originalEntity->id()] = $originalEntity->id();
      }
    }

    // Perform a second filter pass for query parameters.
    // Would be better to filter in a single pass if it is possible.
    $filteredEntityIds = $this->filterContentByQueryParameters($request, $entityIds_unfiltered, $type, $bundle);

    $display = $request->query->get('display') ?? EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID;
    $rendered = [];

    foreach ($filteredEntityIds as $filteredEntityId) {
      // Use $cacheTags parameter if the response needs to be cached.
      $cacheTags = [];
      $fullRenderedEntity = $this->decoupledRenderer->renderByEntityTypeAndId($type, $filteredEntityId, $cacheTags,$display);

      // Let other modules / events perform last changes.
      $this->onBaseEntityRenderBuilt($fullRenderedEntity);

      $rendered[] = $fullRenderedEntity;
    }

    return new JsonResponse($rendered);
  }

  /**
   * Gets content on the specified group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to search on.
   * @param string $type
   *   The entity type of the entities to retrieve on the group.
   * @param string $bundle
   *   The bundle of the entities to retrieve on the group. Must be equal to
   *   $type for bundleless entity types.
   * @param int $offset
   *   Offset content collection retrieval.
   * @param int $limit
   *   Limit content collection retrieval.
   *
   * @return array
   *   Array of IDs.
   *
   * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
   *   Content to retrieve is not configured on the target group type.
   * @throws \Exception
   *   Miscellaneous error such as group module not installed.
   */
  protected function getGroupContentIdsPerGroup(GroupInterface $group, $type, $bundle, $offset, $limit) {
    $groupContentType = $this->getGroupContentTypeOnGroupType($group->getGroupType(), $type, $bundle);

    if (empty($groupContentType)) {
      // No matching GroupContent for the group.
      throw new InvalidParameterException($this->t('The specified content type or bundle is not available on this group.'));
    }

    // This is an ID like: 'group_content_type_123456789abc'.
    $groupContentTypeContentId = $groupContentType->getContentTypeConfigId();

    return $this->getGroupContentStorage()->getQuery()
      ->condition('gid', $group->id())
      ->condition('type', $groupContentTypeContentId)
      ->range($offset, $limit)
      ->execute();
  }

  /**
   * Gets the group content storage.
   *
   * @return \Drupal\group\Entity\Storage\GroupContentStorageInterface
   *   The storage for group content.
   *
   * @throws \Exception
   *   Storage not found. Group module is likely not installed.
   */
  protected function getGroupContentStorage() {
    if (!empty($this->groupContentStorage)) {
      return $this->groupContentStorage;
    }

    try {
      /* @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $groupContentStorage */
      $this->groupContentStorage = $this->entityTypeManager()->getStorage('group_content');
    }
    catch (\Exception $exception) {
      // Group is not installed. Should never happen because of dependency on
      // group module.
      throw new \Exception($this->t('Group is not installed.'));
    }

    return $this->groupContentStorage;
  }

  /**
   * Gets the group content type.
   *
   * This is a GroupContentEnablerInterface plugin object which tells which
   * entity type and bundle are enabled on a group type.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $groupType
   *   The group type on which we want to find the group content type.
   * @param string $type
   *   The group enabled entity type ID. E.g.: 'node'.
   * @param string $bundle
   *   Bundle of the group enabled entity. Must be equal to $type for bundleless
   *   entity types. E.g.: 'article'.
   *
   * @return \Drupal\group\Plugin\GroupContentEnablerInterface|null
   *   The GroupContentEnablerInterface object corresponding to the group, type
   *   and bundle. Returns NULL if not found.
   *
   * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
   */
  protected function getGroupContentTypeOnGroupType(GroupTypeInterface $groupType, $type, $bundle) {
    $installedContentPlugins = $groupType->getInstalledContentPlugins();

    foreach ($installedContentPlugins as $installedContentPlugin) {
      /* @var \Drupal\group\Plugin\GroupContentEnablerInterface $installedContentPlugin */
      $installedContentPlugin->getEntityBundle();

      // Get the entity type managed by this GroupContent.
      $groupContent_entityTypeId = $installedContentPlugin->getEntityTypeId();

      if ($groupContent_entityTypeId !== $type) {
        // Not the right entity type.
        continue;
      }

      // Get the bundle managed by this GroupContent.
      $groupContent_entityBundle = $installedContentPlugin->getEntityBundle();

      if ($groupContent_entityBundle === FALSE) {
        // This GroupContent plugin is for an entity without bundles.
        // In the decoupled_toolbox module, we follow this rule: for entities
        // without bundles, $bundle value must be equal with $type in the query
        // string.
        if ($bundle !== $type) {
          throw new InvalidParameterException($this->t('The entity type @type has no bundles. The bundle parameter must be the same as the type parameter.', ['@type' => $type]));
        }

        // Found our group content (bundleless entity type).
        return $installedContentPlugin;
      }

      if ($groupContent_entityBundle !== $bundle) {
        // Not the right bundle.
        continue;
      }

      // Found our group content (entity type with bundle).
      return $installedContentPlugin;
    }

    // No suitable group content plugin found.
    return NULL;
  }

}
