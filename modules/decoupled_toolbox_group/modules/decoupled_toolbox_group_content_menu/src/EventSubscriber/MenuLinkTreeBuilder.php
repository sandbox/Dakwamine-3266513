<?php

namespace Drupal\decoupled_toolbox_group_content_menu\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\decoupled_toolbox\Event\RenderedOutputEvent;
use Drupal\decoupled_toolbox\Service\DecoupledRendererInterface;
use Drupal\group_content_menu\GroupContentMenuInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;

/**
 * Menu link tree builder for decoupled renderer.
 */
class MenuLinkTreeBuilder implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Menu link tree service for building menu links.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Constructs a new MenuLinkBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTree
   *   Menu link tree service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    MenuLinkTreeInterface $menuLinkTree) {
    $this->entityTypeManager = $entityTypeManager;
    $this->menuLinkTree = $menuLinkTree;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[DecoupledRendererInterface::EVENT__RENDERER__OUTPUT__RENDERED__PREFIX . 'group_content_menu'] = ['onOutputRendered'];

    return $events;
  }

  /**
   * Called when the decoupled renderer has finished rendering a content entity.
   *
   * @param \Drupal\decoupled_toolbox\Event\RenderedOutputEvent $event
   *   The dispatched event.
   */
  public function onOutputRendered(RenderedOutputEvent $event) {
    $entity = $event->getRenderedEntity();

    if ($entity->getEntityTypeId() !== 'group_content_menu') {
      // This should never happen because we already filter by event ID.
      throw new \UnexpectedValueException($this->t('Unexpected entity type when @expected was expected. Given value: @given.', [
        '@expected' => 'group_content_menu',
        '@given' => $entity->getEntityTypeId(),
      ]));
    }

    // The menu name signature comes from the group content menu module.
    $menuName = GroupContentMenuInterface::MENU_PREFIX . $entity->id();

    // Load the menu link tree.
    /* @var \Drupal\group_content_menu\GroupContentMenuInterface $entity */
    $tree = $this->menuLinkTree->load($menuName, (new MenuTreeParameters())->onlyEnabledLinks());

    $treeAsOutput = $this->buildMenuLinkTreeForOutput($tree);

    // Update the render output.
    $output = &$event->getRenderedOutput();
    $output['menu_link_tree'] = $treeAsOutput;

    // Special invalidation tags for menu item links.
    $cacheTags = 'config:system.menu.' . $menuName;
    $finalTags = &$event->getCacheTags();
    $finalTags = Cache::mergeTags($finalTags, [$cacheTags]);
  }

  /**
   * Builds the output of the menu link element.
   *
   * This will build recursively, and the final element is the complete tree.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $menuLinkTreeElement
   *   Element of the tree, as given by MenuLinkTree load function.
   *
   * @return array
   *   The menu link tree element ready for decoupled output.
   */
  private function buildMenuLinkTreeForOutput(array $menuLinkTreeElement) {
    $treeElementOutput = [];

    foreach ($menuLinkTreeElement as $item) {
      /* @var \Drupal\Core\Menu\MenuLinkInterface $link */
      $link = $item->link;
      $currentTreeElementOutput['title'] = $link->getTitle();
      $currentTreeElementOutput['url'] = $link->getUrlObject()->toString();
      $currentTreeElementOutput['options'] = $link->getOptions();

      // Temporarily add weight info.
      $currentTreeElementOutput['weight'] = $link->getWeight();

      if ($item->subtree) {
        $currentTreeElementOutput['branch'] = $this->buildMenuLinkTreeForOutput($item->subtree);
      }
      else {
        $currentTreeElementOutput['branch'] = NULL;
      }

      $treeElementOutput[] = $currentTreeElementOutput;
    }

    // Reorder by weight.
    usort($treeElementOutput, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });

    array_walk($treeElementOutput, function (&$v) {
      // Unset weight, not needed in output.
      unset($v['weight']);
    });

    return $treeElementOutput;
  }

}
