# Introduction

*Decoupled Toolbox* contains common features for decoupled websites.

It makes use of the Field UI and view displays to expose JSON-formatted
content data on a dedicated path.

It is especially useful for scenarios where Drupal is not directly exposed
to the public; where its content is consumed and served by external rendering
services.

> It is not yet recommended to use this module for direct delivery to
frontends. The permission system still needs to be enforced for this use case.

# Requirements

This module has been developed on Drupal core 8.8+. May not work on older
versions.

Sub-modules requirements are explicitly defined on each of them.

# Installation

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

# Usage

## Walkthrough

1. Install the module.
   > A couple of view modes named _Decoupled_ will be created on every
     compatible fieldable content entity type.
2. Go to the view display edit of the fieldable content entity type of your
   choice, e.g. node, taxonomy terms.
3. Go to the _Decoupled_ view mode.
4. Select the fields you want to appear in the JSON output.
5. Configure the formatters to suit your needs. Multiple formatters may be
   available on a same field type.
6. Configure permissions to enable access to the decoupled route (**Access
   decoupled API**) and the entities.
6. Access your decoupled data on this path (replace the placeholders):
   ```
   /decoupled-api/{entity_type}/{bundle}/collection?offset=0&limit=10
   ```

## Output path

The output path follows this pattern:

```
/decoupled-api/{entity_type}/{bundle}/collection?{filters parameters}
```

- `entity_type`: The entity type, e.g. `node`.
- `bundle`: The bundle / vocabulary / content type, e.g `article` for nodes,
  `tags` for taxonomy terms.
  If your entity is not bundleable, simply reuse the `entity_type` value, e.g
  `/decoupled_api/custom_entity/custom_entity/collection?...`.
- `filters parameters`: A list of parameters in usual query style to control
  the output. See *Output filters* section.

## Output filters

By default, only two filter parameters are required:
- `offset`, integer: Controls the search offset, aka the item count to skip.
- `limit`, integer: The item count we want.

You can optionally filter by display:
- `display`, string: machine name of entity display to output (by default is "decoupled")

If you want more precise filtering, you can filter by field values.
The system is analoguous to the entity queries (it is in fact a sort of
wrapper).

To add a field filter, add to the query parameters:
- `filter[{index}][f]={field machine name}`
- `filter[{index}][v]={single value}`
- `filter[{index}][c]={condition}`

Placeholder explanations:
- `index`, integer: For each field filter, you need to set the index, starting
  by 0 for the first field, then 1, then 2, etc.
- `field machine name`, string: The field machine name on the entity.
- `value`, mixed: The value of the filter.
- `condition`, string, optional: One of the possible conditions that are
  offered by entity query: '=', '<>', '>', '>=', '<', '<=', 'STARTS_WITH',
  'CONTAINS', 'ENDS_WITH', 'IN', 'NOT IN', 'BETWEEN'. Uses '=' if not set.

To use the 'IN' and 'NOT IN' conditions, the value syntax is (notice the
brackets):
- `filter[{filter_index}][v][]={value 1}`
- `filter[{filter_index}][v][]={value 2}`
- `filter[{filter_index}][v][]={etc}`

Examples:
- `filter[0][f]=title`
- `filter[0][v]=Title%20of%20the%20content`
- `filter[1][f]=field_tag`
- `filter[1][v][]=10`
- `filter[1][v][]=1337`
- `filter[1][c]=IN`

... which gives in a full path:
```
/decoupled-api/node/article/collection?offset=0&limit=500&filter[0][f]=title&filter[0][v]=Title%20of%20the%20content&filter[1][f]=field_tag&filter[1][v][]=10&filter[1][v][]=1337&filter[1][c]=IN
```

## Decoupled view mode

To format the output, you need to edit the **Decoupled** view mode.

If not present, you can create the _Decoupled_ view mode with the `decoupled`
machine name, and it will be taken into account by the module.

Then, edit the field formatters.

## Decoupled formatters

When you install the module, you start with a set of decoupled formatters.
You must use them instead of the standard formatters.

Using standard formatters will result in no output.

This module also includes several sub-modules on common Drupal contrib field
types. Here is a list of supported field types:

- color_field, via *Decoupled Toolbox for Color Field*
- duration_field, via *Decoupled Toolbox for Duration Field*

Do you need a specific way to output your field? No worries! Because
Decoupled formatters are extendable!

Decoupled formatters are plugins which are similar to standard field
formatters, but with a few adaptations. Go check the current decoupled
formatters in **src/Plugin/Field/FieldFormatter** for examples of
implementation.

Then simply create a custom module, and if you wish to, push it to drupal.org!

## Location solver

This feature lets you relocate the field output somewhere else in the final
entity output.

Let's say you have a title field, and you want it at some other place in the
JSON.

This is what the location solver is for. Explanations on the config form!
You can easely use this feature inside each field formatter (decoupled formatter).
For example a field title with this configuration:

- Decoupled field key: *"title"*
- Decoupled field location:

Will give you the results :

` {"title":"the title value"} `

---

- Decoupled field key: *"title"*
- Decoupled field location: *"parent/subparent"*

Will give you the results :

` {"parent":{"subparent":{"title":"the title value"}}} `

## Other supported specific mechanisms

### Group ecosystem

You want to expose group content handled by *Group*? Please check *Decoupled
Toolbox for Group*.

You want to expose menus created by *Group Content Menu*? Please check
*Decoupled Toolbox for Group Content Menu*.

### Path alias filtering support

You want to filter by the content path alias? Please check *Decoupled Toolbox
— Decoupled Router add-on*.

It requires the *Decoupled Router* contrib module, available at:
https://www.drupal.org/project/decoupled_router.

### Redirect support

You want to expose the source path of a *Redirect* module redirection? Please
check *Decoupled Toolbox for Redirect*.

It makes the source path available in the field UI for your decoupled output.

It works with the *Redirect* contrib module, available at:
https://www.drupal.org/project/redirect.
